package br.com.interaje.petapp.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import br.com.interaje.petapp.R;

/**
 * Created by Edwin on 15/02/2015.
 */
public class TabGato extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.tab_gato, container, false);

        /*
        final ProgressBar progressBar = (ProgressBar) v.findViewById(R.id.progressBar1);
        final TextView tvAndamento = (TextView) v.findViewById(R.id.tv_andamento_numerico);

        final Button btnStartProcess = (Button) v.findViewById(R.id.btn_start_tarefa_tab2);
        btnStartProcess.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnStartProcess.setEnabled(false);
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        int progress = 0;
                        do {
                            progress += (int)(Math.random() * 8);

                            if (progress > 100){
                                progress = 100;
                            }

                            final int progressTemp = progress;

                            progressBar.post(new Runnable() {
                                @Override
                                public void run() {
                                    progressBar.setProgress(progressTemp);
                                    tvAndamento.setText("Andamento: " + progressTemp + "/100");
                                }
                            });

                            try {
                                Thread.sleep(750);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }

                        } while (progress < 100);

                        btnStartProcess.post(new Runnable() {
                            @Override
                            public void run() {
                                btnStartProcess.setEnabled(true);
                                tvAndamento.setText("Tarefa finalizada.");
                            }
                        });
                    }
                }).start();
            }
        });
        */

        return v;
    }


}
