package br.com.interaje.petapp.activities;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import br.com.interaje.petapp.R;
import br.com.interaje.petapp.enums.PetType;
import br.com.interaje.petapp.model.Pet;
import br.com.interaje.petapp.utils.WebServiceUtils;

/**
 * Created by ascaloners on 6/16/15.
 */
public class NewPetActivity extends Activity {
    private Spinner spnPetTypes;
    private Button btnSavePet;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.new_pet);

        spnPetTypes = (Spinner) findViewById(R.id.spn_pet_types);
        spnPetTypes.setAdapter(new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, PetType.values()));
        spnPetTypes.setSelection(0);

        final Activity newPetAct = this;

        btnSavePet = (Button) findViewById(R.id.btn_create_pet);
        btnSavePet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Spinner spnPetType = (Spinner) findViewById(R.id.spn_pet_types);
                final PetType petType = (PetType) spnPetType.getSelectedItem();

                final EditText etPetName = (EditText) findViewById(R.id.et_pet_name);
                if (!etPetName.getText().toString().isEmpty()){
                    final String petName = etPetName.getText().toString();

                    final EditText etPetDescription = (EditText) findViewById(R.id.et_pet_description);
                    if (!etPetDescription.getText().toString().isEmpty()){
                        final String petDescription = etPetDescription.getText().toString();

                        final Pet pet = new Pet(petType, petName, petDescription);
                        btnSavePet.setEnabled(false);
                        btnSavePet.setVisibility(View.INVISIBLE);
                        findViewById(R.id.pb_save_progress).setVisibility(View.VISIBLE);
                        WebServiceUtils.registerPet(pet, newPetAct);
                    }
                }
            }
        });
    }

    public void cleanFields(){
        btnSavePet.setEnabled(true);
        btnSavePet.setVisibility(View.VISIBLE);
        findViewById(R.id.pb_save_progress).setVisibility(View.INVISIBLE);
        ((EditText) findViewById(R.id.et_pet_description)).setText("");
        ((EditText) findViewById(R.id.et_pet_name)).setText("");
        (findViewById(R.id.et_pet_name)).requestFocus();
    }

    @Override
    protected void onPause() {
        super.onPause();
        finish();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
