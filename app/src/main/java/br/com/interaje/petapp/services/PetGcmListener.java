package br.com.interaje.petapp.services;

import android.os.Bundle;
import android.util.Log;

import com.google.android.gms.gcm.GcmListenerService;

import br.com.interaje.petapp.activities.MainActivity;
import br.com.interaje.petapp.model.Pet;

/**
 * Created by ascaloners on 6/25/15.
 */
public class PetGcmListener extends GcmListenerService {

    @Override
    public void onMessageReceived(String from, Bundle data) {
        Log.e(PetGcmListener.class.getName(), "from: "+from+", data: "+data.getString("message"));
        //TODO XXX fix ClasCastException error on pet id
        final Pet pet = new Pet(null,data.getLong("petId"), data.getString("petName"), data.getString("petDescription"));
        Log.i("PET:", pet.toString());
        //TODO XXX create and show notification of new pet
    }
}
