package br.com.interaje.asynctaskexample.services;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.android.gms.gcm.GcmListenerService;

import br.com.interaje.asynctaskexample.R;
import br.com.interaje.asynctaskexample.activities.MainActivity;
import br.com.interaje.asynctaskexample.model.Pet;

/**
 * Created by ascaloners on 6/25/15.
 */
public class PetGcmListener extends GcmListenerService {

    @Override
    public void onMessageReceived(String from, Bundle data) {
        Log.e(PetGcmListener.class.getName(), "from: " + from + ", data: " + data.getString("message"));
        //TODO XXX fix ClasCastException error on pet id
        //TODO XXX fix pet type
        if (data != null){
            final Pet pet = new Pet(null, data.getLong("petId"), data.getString("petName"), data.getString("petDescription"));
            startNotification(pet);
        }
    }

    public void startNotification(final Pet pet) {
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setContentTitle(pet.getName())
                        .setTicker(getResources().getString(R.string.new_pet_msg_notif))
                        .setContentText(pet.getDescription());

        Intent resultIntent = new Intent(this, MainActivity.class);
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
        stackBuilder.addParentStack(MainActivity.class);

        stackBuilder.addNextIntent(resultIntent);
        PendingIntent resultPendingIntent =
                stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
        mBuilder.setContentIntent(resultPendingIntent);
        NotificationManager mNotificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.notify(0, mBuilder.build());
    }
}
