package br.com.interaje.asynctaskexample.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.ProgressBar;

import java.util.ArrayList;
import java.util.List;

import br.com.interaje.asynctaskexample.R;
import br.com.interaje.asynctaskexample.model.Pet;
import br.com.interaje.asynctaskexample.utils.PetMainListAdapter;
import br.com.interaje.asynctaskexample.utils.WebServiceUtils;

/**
 * Created by Edwin on 15/02/2015.
 */
public class TabCachorro extends Fragment {
    private ListView listViewDogs;
    public static ProgressBar progress;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.tab_cachorro, container, false);

        listViewDogs = (ListView) v.findViewById(R.id.lv_dogs);
        final List<Pet> pets = new ArrayList<Pet>();
        final PetMainListAdapter adapter = new PetMainListAdapter(getActivity(), pets);
        listViewDogs.setAdapter(adapter);

        WebServiceUtils.loadDogs(pets, adapter, getActivity());

        progress = (ProgressBar) v.findViewById(R.id.pb_download_dogs);

        return v;
    }
}
