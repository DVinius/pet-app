package br.com.interaje.asynctaskexample.utils;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.DataOutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import br.com.interaje.asynctaskexample.activities.NewPetActivity;
import br.com.interaje.asynctaskexample.activities.SearchPetActivity;
import br.com.interaje.asynctaskexample.activities.TabCachorro;
import br.com.interaje.asynctaskexample.enums.PetType;
import br.com.interaje.asynctaskexample.enums.User;
import br.com.interaje.asynctaskexample.model.Pet;

/**
 * Conjunto de serviços web.
 * <p/>
 * Created by ascaloners on 4/22/15.
 */
public class WebServiceUtils {
    public static RequestQueue queue;
    //public static final String VSG_SERVER = "http://107.170.121.53";
    public static final String VSG_SERVER = "http://192.168.0.33";
    public static final String PORT = "9000";

    public static final String WS_REGISTER_PET = VSG_SERVER + ":" + PORT + "/registerPet";
    public static final String WS_NEXT_PET = VSG_SERVER + ":" + PORT + "/getNextPet";
    public static final String WS_REGISTER_USER_TOKEN = VSG_SERVER + ":" + PORT + "/registerUserToken";
    public static final String WS_SEARCH_PETS = VSG_SERVER + ":" + PORT + "/searchPets";
    public static final String URL_UPLOAD_IMAGE_PET = VSG_SERVER + ":" + PORT + "/upload";

    private static RequestQueue getRequestQueue(final Context context) {
        if (queue == null) {
            queue = Volley.newRequestQueue(context);
        }
        return queue;
    }

    public static void searchPet(final Pet referencePet, final Activity activity) {
        final JSONObject petJson = JSONConverter.petToJson(referencePet);

        final JsonArrayRequest request = new JsonArrayRequest(
                Request.Method.POST,
                WS_SEARCH_PETS,//TODO XXX mudar para buscar considerando os atributos do pet informado
                petJson,
                new Response.Listener<JSONArray>() {

                    //@Override
                    public void onResponse(JSONArray response) {
                        try {

                            for (int index = 0; index < response.length(); index++) {
                                final Pet pet = JSONConverter.responseToPet(response.getJSONObject(index));
                                ((SearchPetActivity) activity).handleSearchResult(pet);
                            }

                        } catch (Exception e) {
                            final String errorMsg = e.getMessage();
                            activity.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Toast.makeText(activity, errorMsg, Toast.LENGTH_LONG).show();
                                }
                            });
                        }
                    }
                }, new Response.ErrorListener() {
            public void onErrorResponse(VolleyError error) {
                final String errorMsg = "Erro durante busca de Pets.";
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(activity, errorMsg, Toast.LENGTH_LONG).show();
                    }
                });
            }
        }
        );
        getRequestQueue(activity).add(request);

    }

    public static void registerPet(final Pet pet, final Activity activity) {
        final JSONObject petJson = JSONConverter.petToJson(pet);

        final JsonObjectRequest request = new JsonObjectRequest(
                Request.Method.POST,
                WS_REGISTER_PET,
                petJson,
                new Response.Listener<JSONObject>() {
                    //@Override
                    public void onResponse(JSONObject response) {
                        try {
                            //crie um mecanismo para converter json para os objetos correspondentes e vice versa.
                            final Long id = Long.parseLong(response.getString("id"));
                            Log.i(WebServiceUtils.class.getName(), "pet remote id: " + id);
                            WebServiceUtils.uploadPetPicture(pet);
                            activity.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Toast.makeText(activity, "Pet cadastrado com sucesso!", Toast.LENGTH_LONG).show();
                                    ((NewPetActivity) activity).cleanFields();
                                }
                            });

                        } catch (Exception e) {
                            final String errorMsg = e.getMessage();
                            activity.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Toast.makeText(activity, errorMsg, Toast.LENGTH_LONG).show();
                                }
                            });
                        }
                    }
                }, new Response.ErrorListener() {
            public void onErrorResponse(VolleyError error) {
                final String errorMsg = "Erro durante cadastro de Pet.";
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(activity, errorMsg, Toast.LENGTH_LONG).show();
                    }
                });
            }
        }
        );
        getRequestQueue(activity).add(request);
    }

    public static void loadDogs(final List<Pet> pets, final PetMainListAdapter adapter, final Activity activity) {
        final Pet fakeDog = new Pet(PetType.DOG, 0l);
        loadPetsByType(fakeDog, pets, adapter, activity);
    }

    public static void loadPetsByType(final Pet referencePet, final List<Pet> pets, final PetMainListAdapter adapter, final Activity activity) {
        final JSONObject petJson = JSONConverter.petToJson(referencePet);

        final JsonObjectRequest request = new JsonObjectRequest(
                Request.Method.POST,
                WS_NEXT_PET,
                petJson,
                new Response.Listener<JSONObject>() {

                    //@Override
                    public void onResponse(JSONObject response) {
                        try {

                            final Long petId = response.getLong("id");
                            if (petId == -1) {
                                activity.runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        TabCachorro.progress.setVisibility(View.INVISIBLE);
                                    }
                                });
                                return;
                            }
                            final Pet loadedPet = JSONConverter.responseToPet(response);
                            if (loadedPet != null) {
                                if (!pets.contains(loadedPet)){
                                    pets.add(loadedPet);
                                }
                            }

                            activity.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    adapter.notifyDataSetChanged();
                                    loadPetsByType(loadedPet, pets, adapter, activity);
                                }
                            });

                        } catch (Exception e) {
                            final String errorMsg = e.getMessage();
                            activity.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Toast.makeText(activity, errorMsg, Toast.LENGTH_LONG).show();
                                }
                            });
                        }
                    }
                }, new Response.ErrorListener() {
            public void onErrorResponse(VolleyError error) {
                final String errorMsg = "Erro durante busca de Pets.";
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(activity, errorMsg, Toast.LENGTH_LONG).show();
                    }
                });
            }
        }
        );
        getRequestQueue(activity).add(request);
    }

    public static void registerUserToken(final String token, final Context context) {
        final Map<String, String> params = new HashMap<>();
        params.put("token", token);
        final JSONObject deviceToken = new JSONObject(params);

        final JsonObjectRequest request = new JsonObjectRequest(
                Request.Method.POST,
                WS_REGISTER_USER_TOKEN,
                deviceToken,
                new Response.Listener<JSONObject>() {

                    //@Override
                    public void onResponse(JSONObject response) {
                        try {
                            final Integer id = response.getInt("id");
                            Log.i(WebServiceUtils.class.getName(), "user device remote id: " + id);
                            User.id = id;

                        } catch (Exception e) {
                            final String errorMsg = e.getMessage();
                            ((Activity) context).runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Toast.makeText(context, errorMsg, Toast.LENGTH_LONG).show();
                                }
                            });
                        }
                    }
                }, new Response.ErrorListener() {
            public void onErrorResponse(VolleyError error) {
                final String errorMsg = "Erro durante registro do token.";
                ((Activity) context).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(context, errorMsg, Toast.LENGTH_LONG).show();
                    }
                });
            }
        }
        );
        getRequestQueue(context).add(request);
    }

    protected static void uploadPetPicture(final Pet pet) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                HttpURLConnection conn = null;
                final String boundary = "*****";
                final String attachmentName = "bitmap";
                final String attachmentFileName = "bitmap.bmp";
                final String crlf = "\r\n";
                try {
                    final URL url = new URL(URL_UPLOAD_IMAGE_PET);
                    conn = (HttpURLConnection) url.openConnection();
                    conn.setRequestMethod("POST");
                    conn.setChunkedStreamingMode(0);
                    conn.setDoOutput(true);
                    conn.setUseCaches(false);
                    conn.setRequestProperty("Connection", "Keep-Alive");
                    conn.setRequestProperty("Cache-Control", "no-cache");
                    conn.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + boundary);
                    conn.connect();

                    final DataOutputStream request = new DataOutputStream(conn.getOutputStream());

                    //request.writeBytes(this.twoHyphens + this.boundary + this.crlf);
                    request.writeBytes("Content-Disposition: form-data; name=\"" + attachmentName + "\";filename=\"" + attachmentFileName + "\"" + crlf);
                    request.write(pet.getImage());
                    request.writeBytes(crlf);
                    request.flush();
                    request.close();

                    /*
                    final InputStream responseStream = new BufferedInputStream(conn.getInputStream());
                    final BufferedReader responseStreamReader = new BufferedReader(new InputStreamReader(responseStream));
                    String line = "";
                    StringBuilder stringBuilder = new StringBuilder();
                    while ((line = responseStreamReader.readLine()) != null) {
                        stringBuilder.append(line).append("\n");
                    }
                    responseStreamReader.close();

                    final String response = stringBuilder.toString();
                    Log.d(WebServiceUtils.class.getName(), "Upload Image response: "+response);
                    */
                } catch (Exception e) {
                    Log.e(WebServiceUtils.class.getName(), "Erro ao realizar upload da imagem do pet: " + e.getMessage());
                } finally {
                    if (conn != null) {
                        conn.disconnect();
                    }
                }
            }
        }).start();
    }

    public static void loadCats(final List<Pet> pets, final PetMainListAdapter adapter, final Activity activity){
        final Pet fakeDog = new Pet(PetType.CAT, 0l);
        loadPetsByType(fakeDog, pets, adapter, activity);
    }
}