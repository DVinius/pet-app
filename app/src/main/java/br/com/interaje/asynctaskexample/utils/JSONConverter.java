package br.com.interaje.asynctaskexample.utils;

import android.util.Base64;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import br.com.interaje.asynctaskexample.enums.PetType;
import br.com.interaje.asynctaskexample.enums.User;
import br.com.interaje.asynctaskexample.model.Pet;

/**
 * Created by Franklin on 4/22/15.
 */
public class JSONConverter {
    private static final String PET_ID = "petId";
    private static final String PET_TYPE = "petType";
    private static final String PET_NAME = "petName";
    private static final String PET_DESCRIPTION = "petDescription";

    public static JSONObject petToJson(final Pet pet){
        final HashMap<String, Object> params = new HashMap<String, Object>();

        params.put("device_id", User.id);
        if (pet.getType() != null){
            params.put("type", pet.getType().ordinal());
        }
        params.put("id", pet.getId());
        params.put("name", pet.getName());
        params.put("description", pet.getDescription());
        if (pet.getLocation() != null){
            params.put("lat", pet.getLocation().latitude);
            params.put("lng", pet.getLocation().longitude);
        }

        return new JSONObject(params);
    }

    public static Pet responseToPet(JSONObject response){
        try {
            final Long petId = response.getLong("id");
            final String petName = response.getString("name");
            final String petDescription = response.getString("description");
            final PetType petType = PetType.getByName(response.getString("type"));
            final LatLng latLng = new LatLng(response.getDouble("lat"), response.getDouble("lng"));

            final Pet pet = new Pet(petType, petId, petName, petDescription);
            pet.setLocation(latLng);
            return pet;
        } catch (JSONException e) {
            Log.e(JSONConverter.class.getName(), "Erro ao converter response to Pet: "+e.getMessage());
            return null;
        }
    }

    /*
    public static JSONObject getContract(final Long lastUnitId){
        final HashMap<String, Object> params = new HashMap<String, Object>();

        //unique device id
        params.put("deviceId", Player.uniqueDeviceId);
        params.put("lastUnitId", lastUnitId);

        return new JSONObject(params);
    }

    public static JSONObject getStats(final Unit unit){
        final HashMap<String, Object> params = new HashMap<String, Object>();

        params.put("remoteId", unit.remoteId);
        params.put("maxHP", unit.maxHP);
        params.put("maxMP", unit.maxMP);
        params.put("regen", unit.regen);
        params.put("mana", unit.mana);
        params.put("strength", unit.strength);
        params.put("intelligence", unit.intelligence);
        params.put("vitality", unit.vitality);
        params.put("spirit", unit.spirit);
        params.put("move", unit.moveCost);
        params.put("agility", unit.agility);

        return new JSONObject(params);
    }

    public static JSONObject getSkill(final Unit unit, final Skill skill){
        final HashMap<String, Object> params = new HashMap<String, Object>();

        //unique device id. Must be send always, to update lastAction!
        params.put("deviceId", Player.uniqueDeviceId);
        params.put("remoteId", unit.remoteId);
        params.put("skillGameId", skill.gameId);
        params.put("skillLevel", skill.level);

        return new JSONObject(params);
    }

    public static JSONObject getPlayerDeviceId(){
        final HashMap<String, Object> params = new HashMap<String, Object>();

        //unique device id. Must be send always, to update lastAction!
        params.put("deviceId", Player.uniqueDeviceId);
        params.put("gcm_reg_id", Player.gcmId);

        return new JSONObject(params);
    }
    */
}