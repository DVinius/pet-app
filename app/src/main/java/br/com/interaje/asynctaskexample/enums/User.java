package br.com.interaje.asynctaskexample.enums;

/**
 * Created by ascaloners on 6/25/15.
 */
public class User {
    public static Integer id;
    public static String deviceToken;

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", deviceToken='" + deviceToken + '\'' +
                '}';
    }
}
