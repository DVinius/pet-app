package br.com.interaje.asynctaskexample.utils;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import br.com.interaje.asynctaskexample.R;
import br.com.interaje.asynctaskexample.model.Pet;

/**
 * Created by ascaloners on 6/25/15.
 */
public class PetMainListAdapter extends BaseAdapter {
    private Context context;
    private List<Pet> pets;
    private LayoutInflater mLayoutInflater = null;

    public PetMainListAdapter(Context context, List<Pet> pets) {
        this.context = context;
        this.pets = pets;
        mLayoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return pets.size();
    }

    @Override
    public Object getItem(int position) {
        return pets.get(position);
    }

    @Override
    public long getItemId(int position) {
        return pets.get(position).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view;
        ViewHolder holder;
        if(convertView == null) {
            view = mLayoutInflater.inflate(R.layout.pet_list_item, parent, false);
            holder = new ViewHolder();
            holder.petPircute = (ImageView)view.findViewById(R.id.iv_pet_picture);
            holder.petName = (TextView)view.findViewById(R.id.tv_pet_name_list_item);
            holder.petDescription = (TextView)view.findViewById(R.id.tv_pet_description_list_item);
            view.setTag(holder);
        } else {
            view = convertView;
            holder = (ViewHolder)view.getTag();
        }

        final Pet pet = pets.get(position);
        holder.petPircute.setImageBitmap(BitmapFactory.decodeResource(context.getResources(), R.mipmap.ic_launcher));
        holder.petName.setText(pet.getName());
        holder.petDescription.setText(pet.getDescription());

        return view;
    }

    private class ViewHolder {
        public ImageView petPircute;
        public TextView petName, petDescription;
    }
}
