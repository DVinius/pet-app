package br.com.interaje.asynctaskexample.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;

import java.io.IOException;

import br.com.interaje.asynctaskexample.R;
import br.com.interaje.asynctaskexample.receivers.MainPetReceiver;
import br.com.interaje.asynctaskexample.utils.MapUtils;
import br.com.interaje.asynctaskexample.utils.WebServiceUtils;

public class MainActivity extends ActionBarActivity {
    Toolbar toolbar;
    ViewPager pager;
    ViewPagerAdapter adapter;
    SlidingTabLayout tabs;
    CharSequence Titles[] = {"Cães", "Gatos"};
    int Numboftabs = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        toolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);

        adapter = new ViewPagerAdapter(getSupportFragmentManager(), Titles, Numboftabs);

        pager = (ViewPager) findViewById(R.id.pager);
        pager.setAdapter(adapter);

        tabs = (SlidingTabLayout) findViewById(R.id.tabs);
        tabs.setDistributeEvenly(true); // To make the Tabs Fixed set this true, This makes the tabs Space Evenly in Available width

        tabs.setCustomTabColorizer(new SlidingTabLayout.TabColorizer() {
            @Override
            public int getIndicatorColor(int position) {
                return getResources().getColor(R.color.tabsScrollColor);
            }
        });

        tabs.setViewPager(pager);

        if (!isGcmRegistered()) {
            new RegisterGcmThread(this).start();
        } else {
            Log.i(MainActivity.class.getName(), "GCM Token already created");
        }

        if (MapUtils.myLastLocation == null){
            new MapUtils(this);
        }
    }

    private Boolean isGcmRegistered() {
        final SharedPreferences sharedPref = getSharedPreferences(
                getString(R.string.preference_file_key), Context.MODE_PRIVATE);
        final String token = sharedPref.getString(getString(R.string.token), "0");
        Log.i(MainActivity.class.getName(), token);
        WebServiceUtils.registerUserToken(token, this);
        return !token.equals("0");
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        switch (id) {
            case R.id.action_settings:
                return true;

            case R.id.action_new_pet:
                if (MainPetReceiver.isNetworkAvailable(this)){
                    startActivity(new Intent(this, NewPetActivity.class));
                } else {
                    Toast.makeText(this, "Desconectado", Toast.LENGTH_LONG).show();
                }
                break;

            case R.id.action_search_pet:
                startActivity(new Intent(this, SearchPetActivity.class));
                break;

        }

        return super.onOptionsItemSelected(item);
    }

    class RegisterGcmThread extends Thread {
        private Context context;

        public RegisterGcmThread(Context context) {
            this.context = context;
        }

        @Override
        public void run() {
            final InstanceID instanceID = InstanceID.getInstance(context);
            try {
                final String token = instanceID.getToken(getString(R.string.gcm_defaultSenderId), GoogleCloudMessaging.INSTANCE_ID_SCOPE, null);
                Log.i(RegisterGcmThread.class.getName(), token);
                final SharedPreferences sharedPrefs = context.getSharedPreferences(context.getString(R.string.preference_file_key), Context.MODE_PRIVATE);
                final SharedPreferences.Editor editor= sharedPrefs.edit();
                editor.putString(context.getString(R.string.token), token);
                editor.apply();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
