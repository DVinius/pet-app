package br.com.interaje.asynctaskexample.activities;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.maps.model.LatLng;

import org.apache.http.entity.StringEntity;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import br.com.interaje.asynctaskexample.R;
import br.com.interaje.asynctaskexample.enums.PetType;
import br.com.interaje.asynctaskexample.model.Pet;
import br.com.interaje.asynctaskexample.utils.FileUtils;
import br.com.interaje.asynctaskexample.utils.MapUtils;
import br.com.interaje.asynctaskexample.utils.WebServiceUtils;

/**
 * Created by ascaloners on 6/16/15.
 */
public class NewPetActivity extends AppCompatActivity {
    private static final int REQUEST_CODE_GET_IMAGE_FILE = 1;
    private Spinner spnPetTypes;
    private Button btnSavePet;
    private ImageView ivPetPicture;
    private Pet pet = new Pet();
    private String filePath;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.new_pet);

        spnPetTypes = (Spinner) findViewById(R.id.spn_pet_types);
        spnPetTypes.setAdapter(new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, PetType.values()));
        spnPetTypes.setSelection(0);

        final Activity newPetAct = this;

        btnSavePet = (Button) findViewById(R.id.btn_create_pet);
        btnSavePet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Spinner spnPetType = (Spinner) findViewById(R.id.spn_pet_types);
                final PetType petType = (PetType) spnPetType.getSelectedItem();

                final EditText etPetName = (EditText) findViewById(R.id.et_pet_name);
                if (!etPetName.getText().toString().isEmpty()){
                    final String petName = etPetName.getText().toString();

                    final EditText etPetDescription = (EditText) findViewById(R.id.et_pet_description);
                    if (!etPetDescription.getText().toString().isEmpty()){
                        final String petDescription = etPetDescription.getText().toString();

                        //pet = new Pet(petType, petName, petDescription);
                        pet.setType(petType);
                        pet.setName(petName);
                        pet.setDescription(petDescription);
                        final LatLng petPosition = new LatLng(MapUtils.myLastLocation.getLatitude(), MapUtils.myLastLocation.getLongitude());
                        pet.setLocation(petPosition);

                        btnSavePet.setEnabled(false);
                        btnSavePet.setVisibility(View.INVISIBLE);
                        findViewById(R.id.pb_save_progress).setVisibility(View.VISIBLE);
                        WebServiceUtils.registerPet(pet, newPetAct);
                    }
                }
            }
        });

        ivPetPicture = (ImageView) findViewById(R.id.iv_pet_picture);
        ivPetPicture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final AlertDialog.Builder builder = new AlertDialog.Builder(newPetAct);
                builder.setTitle("Selecione")
                        .setItems(R.array.picture_origin, new Dialog.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int itemIndex) {
                                switch (itemIndex) {
                                    case 0://from file
                                        pickImage();
                                        break;
                                    case 1://from camera
                                        //startActivity(new Intent(NewPetActivity.this, MainActivity.class));
                                        break;
                                }
                            }
                        });
                final AlertDialog dialog = builder.create();
                dialog.show();
            }
        });

        AdView mAdView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_CODE_GET_IMAGE_FILE && resultCode == Activity.RESULT_OK) {
            try {
                // SDK >= 11 && SDK < 19
                if (Build.VERSION.SDK_INT < 19){
                    filePath = FileUtils.getRealPathFromURI_API11to18(this, data.getData());
                } else{
                    // SDK > 19 (Android 4.4)
                    filePath = FileUtils.getRealPathFromURI_API19(this, data.getData());
                }

                final Bitmap bitmap = FileUtils.rotateImage(filePath);

                ivPetPicture.setImageBitmap(bitmap);

                final ByteArrayOutputStream stream2 = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream2);
                byte[] byteArray = stream2.toByteArray();

                pet.setImage(byteArray);
            } catch (Exception e) {
                Log.e(NewPetActivity.class.getName(), "Erro ao receber imagem do arquivo/camera: "+e.getMessage());
            }
        }
    }

    public void pickImage() {
        final Intent intent = new Intent();

        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);

        startActivityForResult(intent, REQUEST_CODE_GET_IMAGE_FILE);
    }

    public void cleanFields(){
        btnSavePet.setEnabled(true);
        btnSavePet.setVisibility(View.VISIBLE);
        findViewById(R.id.pb_save_progress).setVisibility(View.INVISIBLE);
        ((EditText) findViewById(R.id.et_pet_description)).setText("");
        ((EditText) findViewById(R.id.et_pet_name)).setText("");
        //(findViewById(R.id.et_pet_name)).requestFocus();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
