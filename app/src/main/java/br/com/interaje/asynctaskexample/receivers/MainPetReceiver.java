package br.com.interaje.asynctaskexample.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

public class MainPetReceiver extends BroadcastReceiver{

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.e(MainPetReceiver.class.getName(), "Evento acionado.");
        if (!isNetworkAvailable(context)){
            //Discussão: como proceder agora?
        }

    }

    public static boolean isNetworkAvailable(final Context context) {
        ConnectivityManager cm = (ConnectivityManager)
                context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = cm.getActiveNetworkInfo();
        final boolean connectivityState =
                (networkInfo != null && networkInfo.isConnected());
        return connectivityState;
    }
}
