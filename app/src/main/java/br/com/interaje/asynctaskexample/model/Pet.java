package br.com.interaje.asynctaskexample.model;

import android.location.Location;

import com.google.android.gms.maps.model.LatLng;

import br.com.interaje.asynctaskexample.enums.PetType;

/**
 * Created by ascaloners on 6/15/15.
 */
public class Pet {
    private PetType type;
    private Long id;
    private String name;
    private String description;
    private byte[] image;
    private LatLng location;

    public Pet(PetType type, Long id, String name, String description) {
        this.type = type;
        this.id = id;
        this.name = name;
        this.description = description;
    }

    public Pet(PetType type, String name, String description) {
        this(type, null, name, description);
    }

    public Pet(PetType type, Long id) {
        this.type = type;
        this.id = id;
    }

    public Pet(Long id) {
        this.id = id;
    }

    public Pet(){
        super();
    }

    public PetType getType() {
        return type;
    }

    public void setType(PetType type) {
        this.type = type;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }

    public LatLng getLocation() {
        return location;
    }

    public void setLocation(LatLng location) {
        this.location = location;
    }

    @Override
    public String toString() {
        return "Pet{" +
                "type=" + type +
                ", id=" + id +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                '}';
    }
}
