package br.com.interaje.asynctaskexample.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.ProgressBar;

import java.util.ArrayList;
import java.util.List;

import br.com.interaje.asynctaskexample.R;
import br.com.interaje.asynctaskexample.model.Pet;
import br.com.interaje.asynctaskexample.utils.PetMainListAdapter;
import br.com.interaje.asynctaskexample.utils.WebServiceUtils;

public class TabGato extends Fragment {
    private ListView listView;
    private ProgressBar progressBar;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.tab_gato, container, false);

        listView = (ListView) v.findViewById(R.id.lv_cats);
        final List<Pet> pets = new ArrayList<Pet>();
        final PetMainListAdapter adapter = new PetMainListAdapter(getActivity(), pets);
        listView.setAdapter(adapter);

        WebServiceUtils.loadCats(pets, adapter, getActivity());

        progressBar = (ProgressBar) v.findViewById(R.id.pb_download_cats);

        final SwipeRefreshLayout swipeLayout =
                (SwipeRefreshLayout) v.findViewById(R.id.swipe_cats_layout);
        //opcional
        swipeLayout.setColorSchemeColors(getResources().getColor(R.color.tabsScrollColor));

        swipeLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                //tratar a atualização
                WebServiceUtils.loadCats(pets, adapter, getActivity());
                swipeLayout.setRefreshing(false);
            }
        });

        return v;
    }


}
