package br.com.interaje.asynctaskexample.enums;

import android.util.Log;

/**
 * Created by ascaloners on 6/15/15.
 */
public enum  PetType {
    DOG("Cachorro"),
    CAT("Gato");

    private String name;

    public static PetType getByOrdinal(final int ordinal){
        switch (ordinal){
            case 0: return DOG;
            case 1: return CAT;
            default: Log.e(PetType.class.getName(), "Não existe tipo de pet com o ordinal informado: " + ordinal);
                return null;
        }
    }

    public static PetType getByName(final String name){
        if (name.equals("DOG")){
            return DOG;
        }
        if (name.equals("CAT")){
            return CAT;
        }
        Log.e(PetType.class.getName(), "Tipo de pet nao encontrado: "+name);
        return null;
    }

    PetType(String name){
        this.name = name;
    }

    @Override
    public String toString() {
        return this.name;
    }
}