package br.com.interaje.asynctaskexample.activities;

import android.graphics.BitmapFactory;
import android.location.Location;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import br.com.interaje.asynctaskexample.R;
import br.com.interaje.asynctaskexample.model.Pet;
import br.com.interaje.asynctaskexample.utils.MapUtils;
import br.com.interaje.asynctaskexample.utils.WebServiceUtils;

/**
 * Created by ascaloners on 7/13/15.
 */
public class SearchPetActivity extends AppCompatActivity {
    public static Location myLastLocation;
    private GoogleMap googleMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_pet);

        googleMap = ((MapFragment) getFragmentManager().findFragmentById(R.id.map)).getMap();
        googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);

        if (MapUtils.myLastLocation != null){
            final LatLng latLng = new LatLng(MapUtils.myLastLocation.getLatitude(), MapUtils.myLastLocation.getLongitude() );
            googleMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
            googleMap.animateCamera(CameraUpdateFactory.zoomTo(14));
        }

        //TODO XXX Refactor: change to 'findNearestPets' instead.
        WebServiceUtils.searchPet(new Pet(), this);
    }

    public void handleSearchResult(final Pet pet) {
        googleMap.addMarker(new MarkerOptions()
                .position(pet.getLocation())
                .icon(BitmapDescriptorFactory.fromBitmap(BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher)))
                .title(pet.getName()));
    }
}
